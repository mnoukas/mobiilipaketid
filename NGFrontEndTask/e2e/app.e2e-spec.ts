import { NGFrontEndTaskPage } from './app.po';

describe('ngfront-end-task App', () => {
  let page: NGFrontEndTaskPage;

  beforeEach(() => {
    page = new NGFrontEndTaskPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
