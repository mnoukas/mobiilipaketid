/**
 * Created by Martin on 19.06.2017.
 */

import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/toPromise';

@Injectable()
export class InternetService {

  private internetPacketUrl = 'http://localhost:3000/internet';

  constructor(private http: Http) {}

  getInternetPackets(): Promise<InternetPacket[]> {
    return this.http
      .get(this.internetPacketUrl)
      .toPromise()
      .then((response: Response) => response.json());
  }
}

export class InternetPacket {
  name: string;
  size: string;
  price: number;
  extra: string;
}
