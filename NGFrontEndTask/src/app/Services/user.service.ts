/**
 * Created by Martin on 18.06.2017.
 */

import {Injectable} from "@angular/core";
import {CallPacket} from "./call.service";
import {InternetPacket} from "./internet.service";

@Injectable()
export class UserService {
  constructor() {}}

export class User {
  name = 'Klient';
  numberOfDevices: number;
  internetPacket: InternetPacket;
  callPackets: CallPacket[];
}
