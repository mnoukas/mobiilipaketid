/**
 * Created by Martin on 19.06.2017.
 */

import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/toPromise';

@Injectable()
export class CallService {

  private callPacketUrl = 'http://localhost:3000/call';

  constructor(private http: Http) {}

  getCallPackets(): Promise<CallPacket[]> {
    return this.http
      .get(this.callPacketUrl)
      .toPromise()
      .then((response: Response) => response.json());
  }
}

export class CallPacket {
  name: string;
  description: string;
  price: number;
}
