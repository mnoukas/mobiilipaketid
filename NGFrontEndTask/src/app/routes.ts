/**
 * Created by Martin on 18.06.2017.
 */
import { Routes } from '@angular/router';
import { DeviceComponent } from './device/device.component.';
import { InternetComponent } from './internet/internet.component';
import { CallComponent } from './call/call.component';
import { ReviewComponent } from './review/review.component';
import { IntroComponent } from './intro/intro.component';

export const routes: Routes = [
  { path: '', redirectTo: 'intro', pathMatch: 'full' },
  { path: 'intro', component: IntroComponent },
  { path: 'device', component: DeviceComponent },
  { path: 'internet', component: InternetComponent },
  { path: 'call', component: CallComponent },
  { path: 'review', component: ReviewComponent }
];
