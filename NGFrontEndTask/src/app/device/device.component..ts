import {Component, OnInit} from '@angular/core';
import {User, UserService} from '../Services/user.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.css']
})
export class DeviceComponent implements OnInit {

  constructor(private userService: UserService,
              private router: Router) {}

  ngOnInit() {
    this.checkIfNeedsToBeReset();
  }

  // If page is refreshed, then redirect to beginning
  checkIfNeedsToBeReset(): void {
    if (this.userService.globalUser.numberOfDevices < 1 || this.userService.globalUser.internetPacket == null || this.userService.globalUser.callPackets == null) {
      this.router.navigateByUrl('/intro');
    }
  }

  // Add the number of devices to the global user
  chooseDeviceNumber(number: number) {
    this.userService.globalUser.numberOfDevices = number;
  }
}
