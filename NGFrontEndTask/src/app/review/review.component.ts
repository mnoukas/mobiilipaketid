import {Component, OnInit} from '@angular/core';
import {UserService, User} from '../Services/user.service'
import {CallPacket} from "../Services/call.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  endUser: User = this.userService.globalUser;
  finalCallPackages: CallPacket[] = this.endUser.callPackets;
  totalPrice: number = 0;

  constructor(private userService: UserService,
              private router: Router) {}

  ngOnInit() {
    this.checkIfNeedsToBeReset();
    this.calculateTotalPrice();
  }

  // If page is refreshed, then redirect to beginning
  checkIfNeedsToBeReset(): void {
    if (this.userService.globalUser.numberOfDevices < 1 || this.userService.globalUser.internetPacket == null || this.userService.globalUser.callPackets == null) {
      this.router.navigateByUrl('/intro');
    }
  }

  // Calculate the total price of the internetpacket + callpackage/callpackages
  calculateTotalPrice(): void {
    this.totalPrice += this.endUser.internetPacket.price;
    for (var i = 0; i < this.finalCallPackages.length; i++){
      this.totalPrice += this.finalCallPackages[i].price;
    }
  }

  // Taken from stackoverflow:
  // https://stackoverflow.com/questions/41379274/print-html-template-in-angular-2-ng-print-in-angular-2
  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  };
}
