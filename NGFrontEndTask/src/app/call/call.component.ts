import {Component, OnInit} from '@angular/core';
import {UserService} from '../Services/user.service'
import {CallPacket, CallService} from '../Services/call.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-call',
  templateUrl: './call.component.html',
  styleUrls: ['./call.component.css']
})
export class CallComponent implements OnInit {

  callPackets: CallPacket[] = [];
  chosenCallPackets: CallPacket[] = [];
  isValidNumber: boolean;
  howManyDevices: number = this.userService.globalUser.numberOfDevices;
  chosenCallPacketsFail: boolean;

  constructor(private callService: CallService,
              private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {
    this.checkIfNeedsToBeReset();
    this.showCallPackets();
  }

  // If page is refreshed, then redirect to beginning
  checkIfNeedsToBeReset(): void {
    if (this.userService.globalUser.numberOfDevices < 1 || this.userService.globalUser.internetPacket == null || this.userService.globalUser.callPackets == null) {
      this.router.navigateByUrl('/intro');
    }
  }

  // Get all call packets from db.json
  private showCallPackets(): void {
    this.callService.getCallPackets()
      .then(callPackets => {
        this.callPackets = callPackets;
        this.callPacketPriceChange();
      });
  }

  // When more then 1 device, then change the price of 1 call packet
  private callPacketPriceChange(): void {
    if (this.userService.globalUser.numberOfDevices < 2) {
      this.callPackets[0].price = 1;
    }
  }

  // Check, if global user has selected the same number of call packets as the number of devices he has
  checkIfValidNumberOfCallPackets(): boolean {
    this.isValidNumber = this.chosenCallPackets.length == this.userService.globalUser.numberOfDevices;
    return this.isValidNumber;
  }

  /* Add call packet/packets to the global users choice, don't let the user pick more packets then devices
   and not with the same name, when with the same name then splice it out always.
   */
  chooseCallPackets(newCallPacket: CallPacket) {
    for (let i = 0; i < this.chosenCallPackets.length; i++) {
      if (this.chosenCallPackets[i].name == newCallPacket.name) {
        this.chosenCallPackets.splice(i, 1);
      }
    }
    if (this.userService.globalUser.numberOfDevices > this.chosenCallPackets.length) {
      this.chosenCallPackets.push(newCallPacket);
    } else {
      this.chosenCallPacketsFail = true;
    }
  }

  // Confirm and add selected call packets to the global user, if less than
  confirmCallPackets() {
    this.userService.globalUser.callPackets = this.chosenCallPackets;
  }
}
