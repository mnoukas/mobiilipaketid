import { Component, OnInit } from '@angular/core';
import { UserService } from "../Services/user.service";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  deviceProgress: boolean = false;
  internetProgress: boolean = false;
  callProgress: boolean = false;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.checkIfDevicesHaveBeenChosen();
    this.checkIfInternetPacketHasBeenChosen();
    this.checkIfCallPacketHasBeenChosen();
  }

    checkIfDevicesHaveBeenChosen(): boolean {
      if (this.userService.globalUser.numberOfDevices > 0) {
        this.deviceProgress = true;
      }
      return this.deviceProgress;
    }

    checkIfInternetPacketHasBeenChosen(): boolean {
      if (this.userService.globalUser.internetPacket != null) {
        this.internetProgress = true;
      }
      return this.internetProgress;
    }

    checkIfCallPacketHasBeenChosen(): boolean {
      if (this.userService.globalUser.callPackets != null) {
        this.callProgress = true;
      }
      return this.callProgress;
    }
}
