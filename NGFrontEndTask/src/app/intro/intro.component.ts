import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { UserService } from '../Services/user.service'

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.css']
})
export class IntroComponent implements OnInit {

  constructor(private appComponent: AppComponent,
              private userService: UserService) { }

  ngOnInit() {
  }

  startSession(): void {
    client = new User();
  }
}
