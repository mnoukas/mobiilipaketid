import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AlertModule } from 'ngx-bootstrap'
import {routes} from './routes';
import {RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import { DeviceComponent } from './device/device.component.';
import { InternetComponent } from './internet/internet.component';
import { CallComponent } from './call/call.component';
import { ReviewComponent } from './review/review.component';
import { NavigationComponent } from './navigation/navigation.component';
import { IntroComponent } from './intro/intro.component';


import { UserService } from './Services/user.service';
import { InternetService } from './Services/internet.service';
import { CallService } from './Services/call.service';

@NgModule({
  declarations: [
    AppComponent,
    DeviceComponent,
    InternetComponent,
    CallComponent,
    ReviewComponent,
    NavigationComponent,
    IntroComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes, {useHash: true}),
    AlertModule.forRoot()
  ],
  providers: [UserService, InternetService, CallService],
  bootstrap: [AppComponent]
})
export class AppModule { }
