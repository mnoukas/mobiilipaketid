import {Component, OnInit} from '@angular/core';
import {InternetPacket, InternetService} from '../Services/internet.service';
import {UserService} from '../Services/user.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-internet',
  templateUrl: './internet.component.html',
  styleUrls: ['./internet.component.css']
})
export class InternetComponent implements OnInit {

  internetPackets: InternetPacket[];

  constructor(private internetService: InternetService,
              private userService: UserService,
              private router: Router) {}

  ngOnInit() {
    this.checkIfNeedsToBeReset();
    this.showInternetPackets();
  }

  // If page is refreshed, then redirect to beginning
  checkIfNeedsToBeReset(): void {
    if (this.userService.globalUser.numberOfDevices < 1 || this.userService.globalUser.internetPacket == null || this.userService.globalUser.callPackets == null) {
      this.router.navigateByUrl('/intro');
    }
  }

  // Get all internet packets from db.json
  private showInternetPackets(): void {
    this.internetService.getInternetPackets()
      .then(internetPackets => {
        this.internetPackets = internetPackets;
        this.internetPacketCount();
      });
  }

  // When more then 1 device, then remove 1 internet packet
  private internetPacketCount(): void {
    if (this.userService.globalUser.numberOfDevices > 1) {
      this.internetPackets = this.internetPackets.slice(1, 6)
    }
  }

  // Add an internet packet to the global user
  chooseInternetPacket(newInternetPacket: InternetPacket) {
    this.userService.globalUser.internetPacket = newInternetPacket;
  }
}
